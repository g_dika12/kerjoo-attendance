import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './src copy/model/rootReducer'
import MainNavigation from './src/basic-auth2/navigation/MainNavigation'

const store = createStore(rootReducer, applyMiddleware(thunk));
export class App extends Component {

  render() {
    return (
      
      <Provider store={store}>
        <MainNavigation />
      </Provider>
    )
  }
}

export default App