import React, { Component } from 'react'
import { useState } from 'react';
import { useContext } from 'react';
import { Text, TextInput, View, Button } from 'react-native';

function SignInSceen1() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    // console.log(value)

    return (
        <View style={{ margin: 16 }}>
            <Text> textInComponent - Sign In</Text>

            <TextInput
                placeholder="Username"
                value={username}
                onChangeText={setUsername}
            />
            <TextInput
                placeholder="Password"
                value={password}
                onChangeText={setPassword}
                secureTextEntry
            />
            {/* <Button title="Sign in" onPress={() => signIn({ username, password })} /> */}
        </View>
    )
}

export default SignInSceen1
