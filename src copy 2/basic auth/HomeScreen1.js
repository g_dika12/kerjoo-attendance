import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

export class HomeScreen1 extends Component {
    render() {
        return (
            <View>
                <Text> textInComponent - HomeScreen </Text>
            </View>
        )
    }
}

export default HomeScreen1

const styles = StyleSheet.create({
    center: { flex: 1, alignItems: 'center', justifyContent: 'center' },
})