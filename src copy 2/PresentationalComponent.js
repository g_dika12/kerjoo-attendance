import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

export class PresentationalComponent extends Component {

    render() {
        return (
            <View>
                <Text style={styles.myText} onPress={this.props.updatedText}>{this.props.myText}</Text>
            </View>
        )
    }
}

export default PresentationalComponent

const styles = StyleSheet.create({
    myText:{
        marginTop:20,
        textAlign:'center',
        color:'blue',
        fontWeight:'bold',
        fontSize:20
    }
})