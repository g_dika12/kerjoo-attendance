import React from 'react'

const initialState = {
    isLoading: true,
    isSignout: false,
    userToken: null
}

const reducerLogin = (state = initialState, action) => {
    const prevState = { ...state }
    switch (action.type) {
        case 'RESTORE_TOKEN':
            return {
                ...prevState,
                userToken: action.token,
                isLoading: false
            }
            break;
        case 'SIGN_IN':
            return {
                ...prevState,
                userToken: action.token,
                isSignout: false
            }
            break;
        case 'SIGN_OUT':
            return {
                ...prevState,
                userToken: null,
                isSignout: true
            }
    }
    return prevState;
}

export default reducerLogin;