import React from 'react'
import { combineReducers } from 'redux'
import reducerLogin from './reducerLogin'

const rootReducer = combineReducers({
    login: reducerLogin
})

export default rootReducer;