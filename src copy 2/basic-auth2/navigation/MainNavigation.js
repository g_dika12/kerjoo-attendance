import React, { Component } from 'react'
import { Text, View } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { useDispatch } from 'react-redux'
import { useEffect } from 'react'
import { useMemo } from 'react'

const MainNavigation = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        const storeTokenAsyncstorage = async () => {
            let userToken;
            try {
                userToken = AsyncStorage.getItem('userToken');
                console.log('----?> ', userToken)
                dispatch({ type: 'RESTORE_TOKEN', token: userToken })
            } catch (error) {
                console.log('--?> ', error)
            }
        }
        storeTokenAsyncstorage()
    },[])

    // const authContext = useMemo(
    //     ()=>({

    //     }),
    //     []
    // )
    return (
        <View>
            <Text> textInComponent </Text>
        </View>
    )
}

export default MainNavigation
