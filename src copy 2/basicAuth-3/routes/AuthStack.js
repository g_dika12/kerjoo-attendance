import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import SignInScreen from '../SignInScreen';

const Stack = createStackNavigator();

export const AuthStack=()=>{
    return (
        <Stack.Navigator>
            <Stack.Screen name={'sign in'} component={SignInScreen} />
        </Stack.Navigator>
    )
}