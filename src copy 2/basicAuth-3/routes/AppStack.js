import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import HomeScreen from '../HomeScreen';

const Stack = createStackNavigator();

export const AppStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name={'home'} component={HomeScreen} />
        </Stack.Navigator>
    )
}