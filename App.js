import React, { useEffect } from 'react'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './src/model/rootReducer'
import MainNav from './src/navigation/MainNav'

const store = createStore(rootReducer, applyMiddleware(thunk))
const App = () => {

  return (
    <Provider store={store}>
      <MainNav />
    </Provider>
  )
}

export default App
