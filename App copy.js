/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import ViewScreen from './application/ViewScreen';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import Login from './src/view/Login';
import reducerLogin from './src/model/reducerLogin';
import reducerRegister from './src/model/reducerRegister';
import Register from './src/view/Register';
import rootReducer from './src/model/rootReducer';
import Profile from './src/view/Profile';

// const store = createStore(reducerLogin, applyMiddleware(thunk));
// const storeRegister = createStore(reducerRegister, applyMiddleware(thunk));

const store = createStore(reducerRegister, applyMiddleware(thunk));

// menciptakan penyimpanan diisi dengan reducerLogin, di campur dengan fungsi thunk
// lihat perubahan initialState dari reducerLogin <==> actionLogin.js <<= login.js
//
function App() {
  return (
    <Provider store={store}>
      {/* <Login /> */}
      {/* <Register /> */}
      <Profile />
    </Provider>
  );
}

export default App;