import React, { useEffect, useState, useMemo } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { View, Text, ActivityIndicator } from 'react-native'
import { AuthContext } from './src/components/context';
import { createStackNavigator } from '@react-navigation/stack';
import SignInScreen from './src/screens/SignInScreen';
import HomeScreen from './src/screens/HomeScreen';

const Stack = createStackNavigator()
const App = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [userToken, setUserToken] = useState(null);

  const authContext = useMemo(() => ({
    signIn: () => {
      setTimeout(() => {
        setUserToken('1234asdf');
        setIsLoading(false)
      }, 2000)
    },
    signOut: () => {
      setUserToken(null);
      setIsLoading(false)
    },
    signUp: () => {
      setUserToken('1234asdf');
      setIsLoading(false)
    },
  }), [])

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false)
    }, 4000)
  }, [])

  if (isLoading) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator size={'large'} color={'green'} />
      </View>
    )
  }

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        {
          userToken !== null ?
            <Stack.Navigator>
              <Stack.Screen name={'home'} component={HomeScreen} />
            </Stack.Navigator>
            :
            <Stack.Navigator>
              <Stack.Screen name={'sign in'} component={SignInScreen} />
            </Stack.Navigator>
        }

      </NavigationContainer>
    </AuthContext.Provider>
    // <View>
    //   <Text>Functional component</Text>
    // </View>
  )
}

export default App;