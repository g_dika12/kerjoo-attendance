import React, { Component } from 'react'
import { Button, Text, View } from 'react-native'
import PresentationalComponent from './src/PresentationalComponent';
import TextExample from './src/TextExample';

export class App extends Component {
  constructor(){
    super()
    this.updatedText = this.updatedText.bind(this);
  }
  state = {
    myState:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  }
  updatedText=()=>{this.setState({myState:'text terupdate'})}
  render() {
    return (
      <View style={{margin:16}}>
        <PresentationalComponent 
          myText={this.state.myState}
          updatedText={this.updatedText}
        />
        <TextExample />
      </View>
    )
  }
}

export default App
