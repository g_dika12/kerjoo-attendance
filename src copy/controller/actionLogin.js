export const loginSuccess = (email, password, status) => {
  return {
    type: 'LOGIN_SUCCESS',
    valueEmail: email, // harus sama dgn reducer
    valuePassword: password,
    status: status,
  };
};

export const loginSuccessController = (email, password, status) => {
  return (dispatch) => {
    if (status === 'SUCCESS') {
      dispatch(loginSuccess(email, password, status));
    }
  };
};

export const loginFailed = (email, password, status) => {
  return {
    type: 'LOGIN_FAILED',
    valueEmail: email, // harus sama dgn reducer
    valuePassword: password,
    status: status,
  };
};

export const loginFailedController = (email, password, status) => {
  return (dispatch) => {
    if (status === 'FAILED') {
      dispatch(loginFailed(email, password, status));
    }
  };
};

export const loginUndefined = (email, password, status) => {
  return {
    type: 'LOGIN_UNDEFINED',
    valueEmail: email, // harus sama dgn reducer
    valuePassword: password,
    status: status,
  };
};

export const loginUndefinedController = (email, password, status) => {
  return (dispatch) => {
    if (status === 'UNDEFINED') {
      dispatch(loginUndefined(email, password, status));
    }
  };
};
