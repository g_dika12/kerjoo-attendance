export const registerSuccess = (name, email, password, telephone, status) => {
  return {
    type: 'REGISTER_SUCCESS',
    valueName: name,
    valueEmail: email,
    valuePassword: password,
    valueTelephone: telephone,
    valueStatus: status,
  };
};

export const registerSuccessController = (
  name,
  email,
  password,
  telephone,
  status,
) => {
  return (dispatch) => {
    if (status === 'SUCCESS') {
      dispatch(registerSuccess(name, email, password, telephone, status));
    }
  };
};

export const registerFailed = (name, email, password, telephone, status) => {
  return {
    type: 'REGISTER_FAILED',
    valueName: name,
    valueEmail: email,
    valuePassword: password,
    valueTelephone: telephone,
    valueStatus: status,
  };
};

export const registerFailedController = (
  name,
  email,
  password,
  telephone,
  status,
) => {
  return (dispatch) => {
    if (status === 'FAILED') {
      dispatch(registerFailed(name, email, password, telephone, status));
    }
  };
};

export const registerUndefined = (name, email, password, telephone, status) => {
  return {
    type: 'REGISTER_UNDEFINED',
    valueName: name,
    valueEmail: email,
    valuePassword: password,
    valueTelephone: telephone,
    valueStatus: status,
  };
};

export const registerUndefinedController = (
  name,
  email,
  password,
  telephone,
  status,
) => {
  return (dispatch) => {
    if (status === 'FAILED') {
      dispatch(registerUndefined(name, email, password, telephone, status));
    }
  };
};
