import React from 'react';
import {useEffect, useState} from 'react';
import {View, Text, TextInput, Button, Alert} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {registerSuccessController} from '../controller/actionRegister';
// import {useState} from 'react/cjs/react.production.min';

function Register() {
  const [textInputNama, setTextInputNama] = useState('');
  const [textInputEmail, setTextInputEmail] = useState('');
  const [textInputPassword, setTextInputPassword] = useState('');
  const [textInputTelepon, setTextInputTelepon] = useState('');

  const dispatch = useDispatch();
  // const userName = useSelector((state) => state);
  const {name, email, password, telephone, status} = useSelector((state) => ({
    name: state.register.name,
    email: state.register.email,
    password: state.register.password,
    telephone: state.register.telephone,
    status: state.register.status,
  }));

  useEffect(() => {
    console.log(name); // memanggil initial data sebelum diubah
  }, []);

  const onRegister = () => {
    // Alert('ff');
    dispatch(
      registerSuccessController(
        '12344nama',
        'emailllu',
        'passwordk',
        '098987987',
        'SUCCESS',
      ),
    );
    console.log('--', userName);
  };
  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <Text>Nama</Text>
        <TextInput
          onChangeText={(value) => setTextInputNama(value)}
          placeholder="goo"
        />
      </View>
      <View style={{flexDirection: 'row'}}>
        <Text>Email</Text>
        <TextInput
          onChangeText={(value) => setTextInputEmail(value)}
          placeholder="goo"
        />
      </View>
      <View style={{flexDirection: 'row'}}>
        <Text>Password</Text>
        <TextInput
          onChangeText={(value) => setTextInputPassword(value)}
          placeholder="goo"
        />
      </View>
      <View style={{flexDirection: 'row'}}>
        <Text>No. Telepon</Text>
        <TextInput
          onChangeText={(value) => setTextInputTelepon(value)}
          placeholder="goo"
        />
      </View>
      <Button
        title="Add USER"
        onPress={() => {
          onRegister();
        }}
      />
      <Text>gdg</Text>
    </View>
  );
}

export default Register;
