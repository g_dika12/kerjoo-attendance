import React from 'react';
import {View, Text, Button} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {loginSuccessController} from '../controller/actionLogin';
import {registerSuccessController} from '../controller/actionRegister';

const Profile = () => {
  const {password, email, name, phone} = useSelector((state) => ({
    password: state.login.password,
    email: state.login.email,
    name: state.register.name,
    phone: state.register.telephone,
  }));
  const dispatch = useDispatch();

  const myPressedButton = () => {
    dispatch(
      loginSuccessController('dika.sinaga@gmail.com', '1234567', 'SUCCESS'),
    );
    dispatch(
      registerSuccessController(
        // textInputNama,
        // textInputEmail,
        // textInputPassword,
        // textInputTelepon,
        '12344nama',
        '',
        '',
        '098987987',
        'SUCCESS',
      ),
    );
  };

  return (
    <View>
      <Text>LOGIN password - {password}</Text>
      <Text>LOGIN email - {email}</Text>
      <Text>Register name - {name}</Text>
      <Text>Register phone - {phone}</Text>

      <Button
        title="gooo"
        onPress={() => {
          myPressedButton();
        }}
      />
    </View>
  );
};

export default Profile;
