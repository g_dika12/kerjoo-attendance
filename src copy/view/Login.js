import React, {useState} from 'react';
import {useEffect} from 'react';
import {
  ActivityIndicator,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TextInput} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import {hp, wp} from '../../application/ResponsiveScreen';
import {loginSuccessController} from '../controller/actionLogin';

function Login({navigation}) {
  const [textInputEmail, setTextInputEmail] = useState('');
  const [textInputPassword, setTextInputPassword] = useState('');

  const dispatch = useDispatch();
  const emailUser = useSelector((state) => state.email);
  // console.log(emailUser);

  useEffect(() => {
    console.log(emailUser); // memanggil initial data sebelum diubah
  }, []);

  const signInUser = () => {
    console.log(emailUser);
    dispatch(
      loginSuccessController('dika.sinaga@gmail.com', '1234567', 'SUCCESS'),
    );
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Text style={{textDecorationLine: 'underline', fontWeight: 'bold'}}>
          LOGIN
        </Text>
        <View style={styles.input}>
          <View style={styles.label}>
            <Text>Email</Text>
          </View>
          <TextInput
            style={styles.textInput}
            onChangeText={(value) => setTextInputEmail(value)}
          />
        </View>

        <View style={styles.input}>
          <View style={styles.label}>
            <Text>Password</Text>
          </View>
          <TextInput
            style={styles.textInput}
            onChangeText={(value) => setTextInputPassword(value)}
            secureTextEntry={true}
          />
        </View>

        <TouchableOpacity style={styles.inputButton} onPress={signInUser}>
          <Text style={styles.textLogin}>Login</Text>
        </TouchableOpacity>

        {/* <ActivityIndicator
          size="large"
          color="#00ff00"
          animating={indicatorLoading}
        /> */}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp(100),
    height: hp(100),
    backgroundColor: 'lightgrey',
    alignItems: 'center',
  },
  input: {
    width: wp(95),
    height: hp(10),
    marginTop: hp(0),
  },
  inputButton: {
    width: wp(95),
    height: hp(5),
    marginTop: hp(0),
    alignItems: 'center',
    backgroundColor: 'red',
    justifyContent: 'center',
  },
  inputButtons: {
    width: wp(95),
    height: hp(5),
    marginTop: hp(0),
    alignItems: 'center',
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  label: {
    width: wp(80),
    height: hp(3),
  },
  textInput: {
    width: wp(95),
    height: hp(5),
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
  },
  textInputNumber: {
    width: wp(10),
    height: hp(5),
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
  },
  buttonLogin: {
    backgroundColor: 'green',
    width: wp(100),
  },
  textLogin: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: hp(2),
  },
});

export default Login;
