import React from 'react';

const initialState = {
  name: 'Dika',
  email: 'dikaEmail',
  password: 'dikaPassword',
  telephone: '081234',
  status: 'good',
};

const reducerRegister = (state = initialState, action) => {
  const newState = {...state};
  switch (action.type) {
    case 'REGISTER_SUCCESS':
      newState.name = action.valueName;
      newState.email = action.valueEmail;
      newState.password = action.valuePassword;
      newState.status = action.valueStatus;
      newState.telephone = action.valueTelephone;
      break;
    case 'REGISTER_FAILED':
      newState.name = action.valueName;
      newState.email = action.valueEmail;
      newState.password = action.valuePassword;
      newState.status = action.valueStatus;
      newState.telephone = action.valueTelephone;
      break;
    case 'REGISTER_UNDEFINED':
      newState.name = action.valueName;
      newState.email = action.valueEmail;
      newState.password = action.valuePassword;
      newState.status = action.valueStatus;
      newState.telephone = action.valueTelephone;
      break;
  }
  return newState;
};

export default reducerRegister;
