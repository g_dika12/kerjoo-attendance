
const initialState = {
  email: 'guitherez@gmail.com',
  password: 'belum',
};

const reducerLogin = (state = initialState, action) => {
  const newState = {...state};
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      newState.email = action.valueEmail;
      newState.password = action.valuePassword;
      newState.status = action.valueStatus;
      break;
    case 'LOGIN_FAILED':
      newState.email = action.valueEmail;
      newState.password = action.valuePassword;
      newState.status = action.valueStatus;
      break;
    case 'LOGIN_UNDEFINED':
      newState.email = action.valueEmail;
      newState.password = action.valuePassword;
      newState.status = action.valueStatus;
      break;
  }
  return newState;
};

export default reducerLogin;
