import {combineReducers} from 'redux';
import reducerLogin from './reducerLogin';
import reducerRegister from './reducerRegister';

const rootReducer = combineReducers({
  login: reducerLogin,
  register: reducerRegister,
});

export default rootReducer;
