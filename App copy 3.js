import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen1 from './src/basic auth/HomeScreen1';
import ProfileScreen1 from './src/basic auth/ProfileScreen1';
import SettingScreen1 from './src/basic auth/SettingScreen1';
import SignInSceen1 from './src/basic auth/SignInSceen1'; 
import SignUpScreen1 from './src/basic auth/SignUpScreen1';
import SplashScreen1 from './src/basic auth/SplashScreen1';
import ResetPassword1 from './src/basic auth/ResetPassword1';

import AsyncStorage from '@react-native-async-storage/async-storage' // '@react-native-community/async-storage';
// const [state,dispatch] = 
const Stack = createStackNavigator()
export class App extends Component {

  constructor() {
    super()
    this.state = {
      isLoading:false,
      isSignedIn: false, // === userToken = null
      userToken: null,
    }
  }
  render() {
    if(this.state.isLoading ){
      return <SplashScreen1 />
    }
    return (
      <NavigationContainer>
        {
        // this.state.isSignedIn ?
        this.state.userToken !== null ?
          <Stack.Navigator initialRouteName={'Home1'}>
            <Stack.Screen name="Home1" component={HomeScreen1} />
            <Stack.Screen name="Profile1" component={ProfileScreen1} />
            <Stack.Screen name="Settings1" component={SettingScreen1} />
          </Stack.Navigator>
          :
          <Stack.Navigator initialRouteName={'SignIn'}>
            <Stack.Screen name="SignIn" component={SignInSceen1} />
            <Stack.Screen name="SignUp" component={SignUpScreen1} />
            <Stack.Screen name="ResetPassword" component={ResetPassword1} />
          </Stack.Navigator>
        }
        
      </NavigationContainer>
    )
  }
}

export default App
