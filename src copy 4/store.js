import React, { createContext, useReducer } from 'react'
import loginReducer from './reducerLogin'

const initialLoginState = {
    userToken: null,
    userName: null,
    isLoading: true
}
const Store = ({children}) => {
    const [state, dispatch] = useReducer(loginReducer, initialLoginState)
    return (
        <Context.Provider value={[state, dispatch]}>
            {children}
        </Context.Provider>
    )
}
export const Context = createContext(initialLoginState);

export default Store;