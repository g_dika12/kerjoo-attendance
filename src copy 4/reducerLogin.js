const initialLoginState = {
    userToken: null,
    userName: null,
    isLoading: true
}

const loginReducer = (prevState, action) => {
    switch (action.type) {
        case "RETRIEVE_TOKEN": // untuk cek apakah udah pernah login
            return {
                ...prevState,
                isLoading: false,
                userToken: action.token
            };
        case 'LOGIN':
            return {
                ...prevState,
                isLoading: false,
                userName: action.id,
                userToken: action.token,
            };
        case "LOGOUT":
            return {
                ...prevState,
                isLoading: false,
                userToken: null,
                userName: null
            };
        case "REGISTER":
            return {
                ...prevState,
                isLoading: false,
                userName: action.id,
                userToken: action.token,
            };
    }
}
export default loginReducer;