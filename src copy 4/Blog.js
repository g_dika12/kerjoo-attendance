import axios from 'axios'
import React, { useContext, useEffect } from 'react'
import { View, Text } from 'react-native'
import { Context } from './store'

const Blog = () => {
    const [state, dispatch] = useContext(Context)
    useEffect(() => {
        axios.get('/posts.json')
            .then(response => {
                const postsData = response.data;
                dispatch({ type: 'SET_POSTS', payload: postsData })
            })
            .catch(error => {
                dispatch({ type: 'SET_ERROR', payload: error })
            })
    }, [])
    return (
        <View>
            <Text>Guitherez</Text>
        </View>
    )
}

export default Blog
