import React, { useState } from 'react'
import { Text, View, TextInput, TouchableOpacity } from 'react-native'
import { useDispatch } from 'react-redux'
import { loginActionController } from '../controller/loginAction';

const SigninScreen = () => {

    const dispatch = useDispatch();
    const [data, setData] = useState({
        username: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true
    })

    const textInputChange = (val) => {
        if (val.length !== null) {
            setData({
                ...data,
                username: val,
                check_textInputChange: true
            })
        } else {
            setData({
                ...data,
                username: val,
                check_textInputChange: true
            })
        }
    }

    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val
        })
    }

    const loginHandle = (userName, password) => {
        dispatch(loginActionController(userName, password))
    }

    return (
        <View style={{ margin: 16 }}>
            <Text>Sign in</Text>
            <Text>Name:</Text>
            <TextInput placeholder="username" onChangeText={(val) => textInputChange(val)} />
            <Text>Password:</Text>
            <TextInput placeholder="your password" onChangeText={(val) => handlePasswordChange(val)} autoCapitalize={'none'} secureTextEntry={data.secureTextEmtry ? true : false} />
            <TouchableOpacity>
                <Text style={{ color: '#153e74', marginBottom: 24 }}>Forgot Password ?</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => {
                    loginHandle(data.username, data.password)
                }}
            >
                <View style={{ width: '100%', height: 40, marginBottom: 24, justifyContent: 'center', alignItems: 'center', backgroundColor: 'green' }}>
                    <Text style={{ color: '#fff' }}>Sign In</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={{ width: '100%', height: 40, marginBottom: 24, justifyContent: 'center', alignItems: 'center', borderColor: 'green', borderWidth: 1 }}>
                    <Text style={{ color: 'green' }}>Sign Up</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default SigninScreen