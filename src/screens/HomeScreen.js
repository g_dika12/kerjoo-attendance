import React, { useEffect } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { logoutController } from '../controller/loginAction';

const HomeScreen = () => {
    const dispatch = useDispatch();
    const {token} = useSelector((state)=>({
        token: state.login.userToken,
    }))
    
    return (
        <View style={{margin:16}}>
            <Text>Home Screen - Main Nav - {token}</Text>
            <TouchableOpacity
                onPress={() => {
                    dispatch(logoutController())
                }}
            >
                <View style={styles.box}>
                    <Text style={{ color: 'green' }}>Sign Out</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    box:{ width: '100%', height: 40, marginVertical: 154, justifyContent: 'center', alignItems: 'center', borderColor: 'green', borderWidth: 1 }
})
