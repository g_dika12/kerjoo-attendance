import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { useEffect } from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { registerController, retrieveTokenActionController } from '../controller/loginAction';
import HomeScreen from '../screens/HomeScreen';
import SigninScreen from '../screens/SigninScreen';

const Stack = createStackNavigator()

const MainNav = () => {
    const dispatch = useDispatch();
    const { isLoading, userToken } = useSelector((state) => ({
        isLoading: state.login.isLoading,
        userToken: state.login.userToken
    }))

    useEffect(() => {
        async function userTokenStorage(){
            let userToken;
            try {
                userToken = await AsyncStorage.getItem('userToken')
            } catch (error) {
                console.log(error)
            }
        }
        dispatch(registerController(userToken))
    }, [])

    if (isLoading) {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="green" />
            </View>
        )
    }
    return (
        <NavigationContainer>
            {
                userToken !== null ?
                    <Stack.Navigator>
                        <Stack.Screen name={'home'} component={HomeScreen} />
                    </Stack.Navigator>

                    :
                    <Stack.Navigator>
                        <Stack.Screen name={'sign in'} component={SigninScreen} />
                    </Stack.Navigator>
            }
        </NavigationContainer>
    )
}

export default MainNav
