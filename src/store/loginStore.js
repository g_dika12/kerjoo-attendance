import React, { createContext } from 'react'
import { useReducer } from 'react';
import loginReducer from '../model/loginReducer';

const initialState = {
    isLoading: true,
    userName: null,
    userToken: null
};
const LoginStore = ({ children }) => {
    const [state,dispatch] = useReducer(loginReducer, initialState)
    return (
        <LoginContext.Provider value={[state, dispatch]}>
            {children}
        </LoginContext.Provider>
    )
}
export const LoginContext = createContext(initialState);
export default LoginStore;