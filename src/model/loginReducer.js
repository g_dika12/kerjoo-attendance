const initialState = {
    isLoading: true,
    userName: null,
    userToken: null
}
const loginReducer = (prevState = initialState, action) => {
// const loginReducer = (prevState, action) => { //bila konek ke loginStore;
    switch (action.type) {
        case "RETRIEVE_TOKEN": // untuk cek apakah udah pernah login
            return {
                ...prevState,
                isLoading: false,
                userToken: action.token
            };
        case 'LOGIN':
            return {
                ...prevState,
                isLoading: false,
                userName: action.id,
                userToken: action.token,
            };
        case "LOGOUT":
            return {
                ...prevState,
                isLoading: false,
                userToken: null,
                userName: null
            };
        case "REGISTER":
            return {
                ...prevState,
                isLoading: false,
                userName: action.id,
                userToken: action.token,
            };
    }
    return prevState;
}
export default loginReducer;