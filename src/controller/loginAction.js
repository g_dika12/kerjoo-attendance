import AsyncStorage from "@react-native-async-storage/async-storage"

export const loginAction = (userName, userToken) => {
    return {
        type: 'LOGIN',
        id: userName,
        token: userToken,
        isLoading: false,
    }
}

export const loginActionController = (userName, password) => {
    return async (dispatch) => {
        let userToken;
        userToken = null;
        if (userName == 'user' && password == 'pass') {
            try {
                userToken = '1234asdf99';
                await AsyncStorage.setItem(
                    'userToken',
                    userToken
                )
            } catch (error) {
                console.log(error)
            }
        }
        dispatch(loginAction(userName, userToken))
    }
}

export const retrieveTokenAction = (userToken) => {
    return {
        type: "RETRIEVE_TOKEN",
        isLoading: false,
        token: userToken
    }
}

export const retrieveTokenActionController = (userToken) => {
    return (dispatch) => {
        dispatch(retrieveTokenAction(userToken))
    }
}

export const logoutAction = () => {
    return {
        type: 'LOGOUT',
        isLoading: false,
        userToken: null,
        userName: null
    }
}

export const logoutController = () => {
    return async (dispatch) => {
        try {
            await AsyncStorage.removeItem('userToken')
        } catch (error) {
            console.log(error)
        }
        dispatch(logoutAction())
    }
}

export const registerAction = (userName, userToken) => {
    return {
        type: 'REGISTER',
        id: userName,
        token: userToken
    }
}

export const registerController = (name, token) => {
    return (dispatch) => {
        dispatch(registerAction(name, token))
    }
}