import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { AuthContext } from '../components/context'

const HomeScreen = () => {
    const { signOut } = React.useContext(AuthContext)
    return (
        <View style={{ margin: 16 }}>
            <Text>HomeScreen</Text>
            <TouchableOpacity
                onPress={() => { signOut() }}
            >
                <View style={{ width: '100%', height: 40, marginVertical: 354, justifyContent: 'center', alignItems: 'center', borderColor: 'green', borderWidth: 1 }}>
                    <Text style={{ color: 'green' }}>Sign Out</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default HomeScreen
