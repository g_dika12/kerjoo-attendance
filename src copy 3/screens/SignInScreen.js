import React, { useState } from 'react'
import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import { AuthContext } from '../components/context'

const SignInScreen = () => {
    const { signIn } = React.useContext(AuthContext)
    const [data, setData] = useState({
        username: '',
        password: '',
        check_textInputChange: false,
        secureTextEmtry: true,
    })
    const textInputChange = (val) => {
        if (val.lenght !== null) {
            setData({
                ...data,
                username: val,
                check_textInputChange: true
            })
        } else {
            setData({
                ...data,
                username: val,
                check_textInputChange: true
            })
        }
    }

    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val,
        })
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEmtry: !data.secureTextEmtry,
        })
    }

    return (
        <View style={{ margin: 16 }}>
            <Text>Sign in</Text>
            <Text>Name:</Text>
            <TextInput placeholder="username" onChangeText={(val)=>textInputChange(val)} />
            <Text>Password:</Text>
            <TextInput placeholder="your password" onChangeText={(val)=>handlePasswordChange(val)} autoCapitalize={'none'} secureTextEntry={data.secureTextEmtry? true:false}/>
            <TouchableOpacity>
                <Text style={{ color: '#153e74', marginBottom: 24 }}>Forgot Password ?</Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => { signIn() }}
            >
                <View style={{ width: '100%', height: 40, marginBottom: 24, justifyContent: 'center', alignItems: 'center', backgroundColor: 'green' }}>
                    <Text style={{ color: '#fff' }}>Sign In</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={{ width: '100%', height: 40, marginBottom: 24, justifyContent: 'center', alignItems: 'center', borderColor: 'green', borderWidth: 1 }}>
                    <Text style={{ color: 'green' }}>Sign Up</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default SignInScreen
