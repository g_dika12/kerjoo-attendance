const initialLoginState = {
    userToken: null,
    userName: null,
    isLoading: true
}

const loginReducer = (prevState, action) => {
    switch (action.type) {
        case "RETRIEVE_TOKEN": // untuk cek apakah udah pernah login
            return {
                ...prevState,
                isLoading: false,
                userToken: action.token
            };
        case 'LOGIN':
            return {
                ...prevState,
                isLoading: false,
                userName: action.id,
                userToken: action.token,
            };
        case "LOGOUT":
            return {
                ...prevState,
                isLoading: false,
                userToken: null,
                userName: null
            };
        case "REGISTER":
            return {
                ...prevState,
                isLoading: false,
                userName: action.id,
                userToken: action.token,
            };
    }
}

// const [loginState, dispatch] = React.useReducer(loginReducer, initialLoginState);

// const authContext = React.useMemo(() => {
//     signIn: (userName, password) => {
//         let userToken;
//         userName = null;
//         if (userName == 'user' && password == 'pass') {
//             userToken = '1234asdf'
//         }
//         dispatch({ type: 'LOGIN', id: userName, token: userToken });
//     };
//     signOut: () => {
//         dispatch({ type: 'LOGOUT' });
//     };

// }, [])