import React, { useState } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Modal, Platform } from 'react-native';
import { TextInput, Button, RadioButton } from 'react-native-paper';
import { hp, wp } from './ResponsiveScreen';
import axios from 'axios';


function Register() {

    const [textInputEmail, setTextInputEmail] = useState('');
    const [textInputPassword, setTextInputPassword] = useState('');
    const [textInputUsername, setTextInputUsername] = useState('');
    const [tokenKey, setTokenKey] = useState('');

    const register = async () => {
        try {
            const result = await axios.post('http://18.139.50.74:8080/register', {
                email: textInputEmail,
                usrename: textInputUsername,
                password: textInputPassword
            });

            console.log(result.data);

            // if (result.data.status == 'Login Success') {
            //     SharedPrefences.setItem('token', result.data.token);
            //     SharedPrefences.getItem('token', function (value) {
            //         setTokenKey(value);
            //     })
            //     navigation.navigate('List');
            // }
        } catch (err) {
            console.error(err);
        }

    }


    return (
        <SafeAreaView>
            <View style={styles.container}>
                <Text style={{ textDecorationLine: 'underline', fontWeight: 'bold' }}>REGISTER</Text>
                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Email</Text>
                    </View>
                    <TextInput style={styles.textInput} onChangeText={value => setTextInputEmail(value)} />
                </View>

                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Username</Text>
                    </View>
                    <TextInput style={styles.textInput} onChangeText={value => setTextInputEmail(value)} />
                </View>

                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Password</Text>
                    </View>
                    <TextInput
                        style={styles.textInput}
                        onChangeText={value => setTextInputPassword(value)}
                        secureTextEntry={true}
                    />
                </View>

                <TouchableOpacity style={styles.inputButton} onPress={register}>
                    <Text style={styles.textRegister}>Register</Text>
                </TouchableOpacity>

                {/* <TouchableOpacity style={styles.inputButtons} onPress={getData}>
                    <Text style={styles.textLogin}>GET DATA</Text>
                </TouchableOpacity> */}
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        width: wp(100),
        height: hp(100),
        backgroundColor: 'lightgrey',
        alignItems: 'center'
    },
    input: {
        width: wp(95),
        height: hp(10),
        marginTop: hp(0)
    },
    inputButton: {
        width: wp(95),
        height: hp(5),
        marginTop: hp(0),
        alignItems: 'center',
        backgroundColor: 'red',
        justifyContent: 'center'
    },
    inputButtons: {
        width: wp(95),
        height: hp(5),
        marginTop: hp(0),
        alignItems: 'center',
        backgroundColor: 'green',
        justifyContent: 'center'
    },
    label: {
        width: wp(80),
        height: hp(3)
    },
    textInput: {
        width: wp(95),
        height: hp(5),
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 1
    },
    textInputNumber: {
        width: wp(10),
        height: hp(5),
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 1
    },
    buttonRegister: {
        backgroundColor: 'green',
        width: wp(100)
    },
    textRegister: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: hp(2)
    },
});

export default Register;