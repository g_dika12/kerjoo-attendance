import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Modal,
  Platform,
  ActivityIndicator,
  PermissionsAndroid,
} from 'react-native';
import {TextInput, Button, RadioButton} from 'react-native-paper';
import {hp, wp} from './ResponsiveScreen';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Geolocation from 'react-native-geolocation-service';

function Attendance({navigation}) {
  const [status, setStatus] = useState('-');
  const [accessToken, setAccessToken] = useState('');
  const [latitude, setLatitude] = useState('');
  const [longitude, setLongitude] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [currentDate, setCurrentDate] = useState('');
  const [currentTime, setCurrentTime] = useState('');
  const [indicatorLoading, setIndicatorLoading] = useState(true);
  const [granted, setGranted] = useState();

  useEffect(() => {
    let unmounted = false;
    getAccessToken();
    getPermission();
    if (getCurrentTime() === true && getCurrentDate() === true) {
      setIndicatorLoading(false);
    }
    return () => {
      unmounted = true;
    };
  }, []);

  const getPermission = async () => {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Need Permission',
        message: 'App access to your location ',
      },
      PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      {
        title: 'Need Permission',
        message: 'App access to your location ',
      },
    );

    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      getCurrentLocation();
    } else {
      console.log('location permission denied');
      alert('Location permission denied');
    }
  };

  const getAccessToken = async () => {
    try {
      const value = await AsyncStorage.getItem('@access_token');
      if (value !== null) {
        // console.log(value)
        setAccessToken(value);
      }
    } catch (err) {
      // error reading value
      console.log(err);
      setAccessToken('');
    }
  };

  const getCurrentDate = () => {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }

    today = yyyy + '-' + mm + '-' + dd;

    console.log(today);

    setDate(today.toString());
    setCurrentDate(today.toString());

    return true;
  };

  const getCurrentTime = () => {
    let time = new Date();
    let hour = time.getHours();
    let minutes = time.getMinutes();
    let second = time.getSeconds();

    if (hour < 10) {
      hour = '0' + hour;
    }

    if (minutes < 10) {
      minutes = '0' + minutes;
    }

    if (second < 10) {
      second = '0' + second;
    }

    time = hour + ':' + minutes + ':' + second;
    console.log(time);
    setTime(time.toString());
    setCurrentTime(time.toString());
    return true;
  };

  const getCurrentLocation = async () => {
    try {
      Geolocation.getCurrentPosition(
        (position) => {
          setLatitude(position.coords.latitude.toString());
          setLongitude(position.coords.longitude.toString());
          console.log('latitude:' + position.coords.latitude.toString());
          console.log('longitude:' + position.coords.latitude.toString());
        },
        (error) => {
          // See error code charts below.
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (err) {
      console.warn(err);
    }

    return true;
  };

  const attendanceIn = async () => {
    try {
      const res = await axios.post(
        'https://apitest.kerjoo.com/api/v1/attendances',
        {
          type_id: '1',
          log_date: date, //masukan tanggal terkini dng format Y-m-d
          log_time: time, //masukan waktu terkini dng format H:i:s
          longitude: longitude, //masukan lokasi longitude terkini yg didapat dari library react-native-geo-location-services
          latitude: latitude, //masukan lokasi latitude terkini yg didapat dari library react-native-geo-location-services
        },
        {
          headers: {Authorization: `Bearer ${accessToken}`}, //masukan access_token_yg_didapat saat login
        },
      );
      console.log(res.data);
      alert('Absen masuk aktif');
      setStatus('masuk');
      getCurrentTime();
      getCurrentDate();
    } catch (err) {
      console.error(err);
      alert(err);
    }
  };

  //fungsi untuk absen keluar
  const attendanceOut = async () => {
    try {
      const res = await axios.post(
        'https://apitest.kerjoo.com/api/v1/attendances',
        {
          type_id: '2',
          log_date: date, //masukan tanggal terkini dng format Y-m-d
          log_time: time, //masukan waktu terkini dng format H:i:s
          longitude: longitude, //masukan lokasi longitude terkini yg didapat dari library react-native-geo-location-services
          latitude: latitude, //masukan lokasi latitude terkini yg didapat dari library react-native-geo-location-services
        },
        {
          headers: {Authorization: `Bearer ${accessToken}`}, //masukan access_token_yg_didapat saat login
        },
      );
      console.log(res.data);
      alert('Absen keluar aktif');
      setStatus('keluar');
      getCurrentTime();
      getCurrentDate();
    } catch (err) {
      console.error(err);
      alert(err);
    }
  };

  //fungsi untuk absen istirahat
  const attendanceBreak = async () => {
    try {
      const res = await axios.post(
        'https://apitest.kerjoo.com/api/v1/attendances',
        {
          type_id: '3',
          log_date: date, //masukan tanggal terkini dng format Y-m-d
          log_time: time, //masukan waktu terkini dng format H:i:s
          longitude: longitude, //masukan lokasi longitude terkini yg didapat dari library react-native-geo-location-services
          latitude: latitude, //masukan lokasi latitude terkini yg didapat dari library react-native-geo-location-services
        },
        {
          headers: {Authorization: `Bearer ${accessToken}`}, //masukan access_token_yg_didapat saat login
        },
      );
      console.log(res.data);
      alert('Absen istirahat aktif');
      setStatus('istirahat');
      getCurrentTime();
      getCurrentDate();
    } catch (err) {
      console.error(err);
      alert(err);
    }
  };

  //fungsi untuk absen selesai istirahat
  const attendanceAfterBreak = async () => {
    try {
      const res = await axios.post(
        'https://apitest.kerjoo.com/api/v1/attendances',
        {
          type_id: '4',
          log_date: date, //masukan tanggal terkini dng format Y-m-d
          log_time: time, //masukan waktu terkini dng format H:i:s
          longitude: longitude, //masukan lokasi longitude terkini yg didapat dari library react-native-geo-location-services
          latitude: latitude, //masukan lokasi latitude terkini yg didapat dari library react-native-geo-location-services
        },
        {
          headers: {Authorization: `Bearer ${accessToken}`}, //masukan access_token_yg_didapat saat login
        },
      );
      alert('Absen selesai istirahat aktif');
      setStatus('selesai istirahat');
      console.log(res.data);
      getCurrentTime();
      getCurrentDate();
    } catch (err) {
      console.error(err);
      alert(err);
    }
  };

  //fungsi untuk absen mulai lembur
  const attendanceOverTime = async () => {
    try {
      const res = await axios.post(
        'https://apitest.kerjoo.com/api/v1/attendances',
        {
          type_id: '5',
          log_date: date, //masukan tanggal terkini dng format Y-m-d
          log_time: time, //masukan waktu terkini dng format H:i:s
          longitude: longitude, //masukan lokasi longitude terkini yg didapat dari library react-native-geo-location-services
          latitude: latitude, //masukan lokasi latitude terkini yg didapat dari library react-native-geo-location-services
        },
        {
          headers: {Authorization: `Bearer ${accessToken}`}, //masukan access_token_yg_didapat saat login
        },
      );
      console.log(res.data);
      alert('Absen mulai lembur aktif');
      setStatus('mulai lembur');
      getCurrentTime();
      getCurrentDate();
    } catch (err) {
      alert(err);
      console.error(err);
    }
  };

  // untuk absen selesai lembur
  const attendanceOvertimeOut = async () => {
    try {
      const res = await axios.post(
        'https://apitest.kerjoo.com/api/v1/attendances',
        {
          type_id: '6',
          log_date: date, //masukan tanggal terkini dng format Y-m-d
          log_time: time, //masukan waktu terkini dng format H:i:s
          longitude: longitude, //masukan lokasi longitude terkini yg didapat dari library react-native-geo-location-services
          latitude: latitude, //masukan lokasi latitude terkini yg didapat dari library react-native-geo-location-services
        },
        {
          headers: {Authorization: `Bearer ${accessToken}`}, //masukan access_token_yg_didapat saat login
        },
      );
      console.log(res.data);
      alert('selesai lembur aktif');
      setStatus('Absen selesai lembur');
      getCurrentTime();
      getCurrentDate();
    } catch (err) {
      console.error(err);
      alert(err);
    }
  };

  // untuk absen selesai lembur
  const attendanceLogout = async () => {
    try {
      const res = await axios.post(
        'https://apitest.kerjoo.com/api/v1/auth/logout',
        {},
        {
          headers: {Authorization: `Bearer ${accessToken}`}, //masukan access_token_yg_didapat saat login
        },
      );
      console.log(res.data);

      if (res.data.success === true) {
        navigation.navigate('Login');
      }
    } catch (err) {
      console.error(err);
      alert(err);
    }
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.header}>
          <Button
            style={{backgroundColor: 'white', marginTop: hp(2)}}
            onPress={attendanceLogout}>
            {' '}
            KELUAR
          </Button>
        </View>
        <View style={styles.attendanceStatus}>
          <ActivityIndicator
            size="large"
            color="#00ff00"
            animating={indicatorLoading}
          />
          <Text>Status absen: {status}</Text>
          <Text>Pada jam: {currentTime}</Text>
          <Text>Pada tanggal: {currentDate}</Text>
        </View>
        <View style={styles.attendanceList}>
          <Button
            color="white"
            style={{backgroundColor: 'blue'}}
            onPress={attendanceIn}>
            ABSEN MASUK
          </Button>
          <Button
            color="white"
            style={{backgroundColor: 'red', marginTop: hp(2)}}
            onPress={attendanceOut}>
            ABSEN KELUAR
          </Button>
          <Button
            color="white"
            style={{backgroundColor: 'grey', marginTop: hp(2)}}
            onPress={attendanceBreak}>
            ABSEN ISTIRAHAT
          </Button>
          <Button
            color="white"
            style={{backgroundColor: 'cyan', marginTop: hp(2)}}
            onPress={attendanceAfterBreak}>
            ABSEN SELESAI ISTIRAHAT
          </Button>
          <Button
            color="white"
            style={{backgroundColor: 'violet', marginTop: hp(2)}}
            onPress={attendanceOverTime}>
            ABSEN MULAI LEMBUR
          </Button>

          <Button
            color="white"
            style={{backgroundColor: 'black', marginTop: hp(2)}}
            onPress={attendanceOvertimeOut}>
            ABSEN SELESAI LEMBUR
          </Button>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  header: {
    width: wp(100),
    height: hp(10),
    backgroundColor: 'white',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  container: {
    width: wp(100),
    height: hp(100),
    backgroundColor: 'lightgrey',
    alignItems: 'center',
  },
  attendanceList: {
    width: wp(100),
    height: hp(50),
    backgroundColor: 'white',
    alignItems: 'center',
  },
  attendanceStatus: {
    width: wp(100),
    height: hp(40),
    backgroundColor: 'lightgrey',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Attendance;
