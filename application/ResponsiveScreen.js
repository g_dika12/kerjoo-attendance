import { Dimensions } from "react-native"

export const wp = (param) => {

    if (param <= 100) {
        let width = Dimensions.get('window').width;
        let responsiveWidth = (param/100) * width; 
        return responsiveWidth;  
    }
    if (param > 100) {
        let responsiveWidth = 0; 
        return responsiveWidth;
    }
}

export const hp = (param) => {

    if (param <= 100) {
        let height = Dimensions.get('window').height;
        let responsiveHeight = (param/100) * height; 
        return responsiveHeight;  
    }
    if (param > 100) {
        let responsiveHeight = 0; 
        return responsiveHeight;
    }
}