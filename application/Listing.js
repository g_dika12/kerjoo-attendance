import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Modal,
  RefreshControl,
} from 'react-native';
import {
  ActivityIndicator,
  Button,
  Card,
  Dialog,
  FAB,
  Paragraph,
  Searchbar,
  List,
  RadioButton,
} from 'react-native-paper';
import axios from 'axios';
import {wp, hp} from './ResponsiveScreen';
import Icon from 'react-native-vector-icons/FontAwesome';

function Listing() {
  const [data, setData] = useState([]);

  const [tokenKey, setTokenKey] = useState('');

  useEffect(() => {
    let unmounted = false;

    // navigation.addListener('focus', () => {
    // SharedPrefences.getItem('token', function (value) {
    //     setTokenKey(value);
    //     axios.get('http://18.139.50.74:8080/checklist', {
    //         headers: { 'Authorization': `Bearer ${value}` }
    //     })
    //         .then(res => {
    //             // setIsLoading(false);

    //             const data = res.data;
    //             // console.log(data["data"]);
    //             setData(data["data"]);
    //             // console.log(res.data['id'])
    //         })
    //         .catch(err => {
    //             console.error(err);
    //         });
    // });
    // });

    return () => {
      unmounted = true;
    };
  }, []);

  const checkList = (param) => {
    SharedPrefences.getItem('token', function (value) {
      setTokenKey(value);
      axios
        .post(
          'http://18.139.50.74:8080/checklist',
          {
            name: param,
          },
          {
            headers: {Authorization: `Bearer ${value}`},
          },
        )
        .then((res) => {
          alert('Berhasil menambah ke List');
        })
        .catch((err) => {
          console.error(err);
        });
    });
  };

  const deleteList = (id) => {
    SharedPrefences.getItem('token', function (value) {
      setTokenKey(value);

      axios
        .delete('http://18.139.50.74:8080/checklist/' + id, {
          headers: {Authorization: `Bearer ${value}`},
        })
        .then((res) => {
          alert('Berhasil menghapus');
        })
        .catch((err) => {
          console.error(err);
        });
    });
  };

  const renderItem = ({item}) => {
    const itemList = item.items;
    // console.log(item.items[0].id)
    return (
      <Card style={styles.card}>
        <Card.Content>
          <Text>ID: {`${item.id}`}</Text>
          <Text>Name: {`${item.name}`}</Text>
        </Card.Content>
        <Card.Actions>
          <Button
            style={styles.buttonEdit}
            labelStyle={styles.labelButtonEdit}
            onPress={() => checkList(item.name)}>
            Checklist
          </Button>
          <Button
            style={styles.buttonDelete}
            labelStyle={styles.labelButtonDelete}
            onPress={() => deleteList(item.id)}>
            DELETE
          </Button>
        </Card.Actions>
      </Card>
    );
  };

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <FlatList
          keyExtractor={(item) => item.id.toString()}
          data={data}
          renderItem={renderItem}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  containerLoading: {
    width: wp(100),
    height: hp(100),
    backgroundColor: 'lightgrey',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    width: wp(100),
    height: hp(100),
    backgroundColor: 'lightgrey',
    alignItems: 'center',
  },
  title: {
    backgroundColor: 'white',
    width: wp(100),
    height: hp(7),
    marginTop: hp(2),
    justifyContent: 'center',
  },
  sortList: {
    backgroundColor: 'white',
    width: wp(100),
    height: hp(7),
    marginTop: hp(2),
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemSortList: {
    width: wp(95),
    height: hp(5),
    justifyContent: 'center',
    borderColor: 'black',
    borderWidth: 1,
    flexDirection: 'row',
  },
  list: {
    width: wp(95),
    height: hp(70),
  },
  textTitle: {
    textAlign: 'center',
    fontWeight: 'bold',
  },
  card: {
    marginTop: hp(2),
  },
  fab: {
    position: 'absolute',
    margin: 15,
    right: 0,
    bottom: hp(10),
  },
  buttonEdit: {
    backgroundColor: 'blue',
  },
  buttonDelete: {
    backgroundColor: 'red',
    marginLeft: wp(3),
  },
  labelButtonEdit: {
    color: 'white',
  },
  labelButtonDelete: {
    color: 'white',
  },
  buttonLoadMore: {
    backgroundColor: 'yellow',
  },
  containerModal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },

  listItem: {
    // backgroundColor:'green',
    height: hp(5),
    flexDirection: 'row',
  },
  listLabel: {
    // backgroundColor:'orange',
    width: '20%',
  },
  listChoice: {
    // backgroundColor:'salmon',
    width: '80%',
    paddingTop: '1.5%',
  },
});

export default Listing;
