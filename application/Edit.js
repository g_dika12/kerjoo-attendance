import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Modal } from 'react-native';
import { TextInput, Button, RadioButton } from 'react-native-paper';
import { hp, wp } from '../ResponsiveScreen';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';

function EditEmployees({ route, navigation}) {

    const [text, setText] = useState('');
    const [visibleSelectRole, setVisibleSelectRole] = useState(false); 
    const [visibleSelectLevel, setVisibleSelectLevel] = useState(false);
    const [visibleSelectLastEducation, setVisibleSelectLastEducation] = useState(false);


    const [textInputEmployeesId, setTextInputEmployeesId] = useState('');
    const [textInputEmployeesName, setTextInputEmployeesName] = useState('');
    const [textInputEmployeesAge, setTextInputEmployeesAge] = useState('');
    const [textInputEmployeesRole, setTextInputEmployeesRole] = useState('');
    const [textInputEmployeesLevel, setTextInputEmployeesLevel] = useState('');
    const [textInputEmployeesAddress, setTextInputEmployeesAddress] = useState('');
    const [textInputEmployeesLastEducation, setTextInputEmployeesLastEducation] = useState('');
    const [textInputEmployeesSalary, setTextInputEmployeesSalary] = useState('');
    

    const { 
        idEmployees,
        nameEmployees,
        ageEmployees,
        roleEmployees,
        levelEmployees,
        addressEmployees,
        lastEducationEmployees,
        salaryEmployees
     } = route.params;

    useEffect(() => {
        setTextInputEmployeesId(idEmployees);
        setTextInputEmployeesName(nameEmployees);
        setTextInputEmployeesAge(ageEmployees);
        setTextInputEmployeesRole(roleEmployees);
        setTextInputEmployeesLevel(levelEmployees);
        setTextInputEmployeesAddress(addressEmployees);
        setTextInputEmployeesLastEducation(lastEducationEmployees);
        setTextInputEmployeesSalary(salaryEmployees);

    },[])


    const selectRole = () => {
        var modalBackgroundStyle = { backgroundColor: 'rgba(0, 0, 0, 0.5)' };
        var innerContainerTransparentStyle = { backgroundColor: '#fff', padding: 20, height: '50%', width: '80%', flexDirection: 'column', paddingBottom: '5%' };
        return (
            <Modal
                animationType='slide'
                transparent={true}
                visible={visibleSelectRole}
                onRequestClose={false}
            >
                <View style={[styles.containerModal, modalBackgroundStyle]}>
                    <View style={innerContainerTransparentStyle}>
                        <Text style={{ textDecorationLine: 'underline' }} onPress={() => setVisibleSelectRole(false)}>CLOSE</Text>
                        <View style={styles.list}>
                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Front End Web Developer'
                                        status={textInputEmployeesRole === 'Front End Web Developer'? 'checked' : 'unchecked'}
                                        onPress={() => chooseRole('Front End Web Developer')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseRole('Front End Web Developer')}>Front End Web Developer</Text>
                                </View>
                            </View>

                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Back End Developer'
                                        status={textInputEmployeesRole === 'Back End Developer'? 'checked' : 'unchecked'}
                                        onPress={() => chooseRole('Back End Developer')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseRole('Back End Developer')}>Back End Developer</Text>
                                </View>
                            </View>

                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Mobile Developer'
                                        status={textInputEmployeesRole === 'Mobile Developer'? 'checked' : 'unchecked'}
                                        onPress={() => chooseRole('Mobile Developer')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseRole('Mobile Developer')}>Mobile Developer</Text>
                                </View>
                            </View>

                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Quality Assurance'
                                        status={textInputEmployeesRole === 'Quality Assurance'? 'checked' : 'unchecked'}
                                        onPress={() => chooseRole('Quality Assurance')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text  onPress={() => chooseRole('Quality Assurance')}>Quality Assurance</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }

    const chooseRole = (param) => {
        setTextInputEmployeesRole(param);
        setVisibleSelectRole(false);
    }

    const selectLevel = () => {
        var modalBackgroundStyle = { backgroundColor: 'rgba(0, 0, 0, 0.5)' };
        var innerContainerTransparentStyle = { backgroundColor: '#fff', padding: 20, height: '50%', width: '80%', flexDirection: 'column', paddingBottom: '5%' };
        return (
            <Modal
                animationType='slide'
                transparent={true}
                visible={visibleSelectLevel}
                onRequestClose={false}
            >
                <View style={[styles.containerModal, modalBackgroundStyle]}>
                    <View style={innerContainerTransparentStyle}>
                        <Text style={{ textDecorationLine: 'underline' }} onPress={() => setVisibleSelectLevel(false)}>CLOSE</Text>
                        <View style={styles.list}>
                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Junior'
                                        status={textInputEmployeesLevel === 'Junior'? 'checked' : 'unchecked'}
                                        onPress={() => chooseLevel('Junior')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseLevel('Front End Web Developer')}>Junior</Text>
                                </View>
                            </View>

                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Middle'
                                        status={textInputEmployeesLevel === 'Middle'? 'checked' : 'unchecked'}
                                        onPress={() => chooseLevel('Middle')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseLevel('Middle')}>Middle</Text>
                                </View>
                            </View>

                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Senior'
                                        status={textInputEmployeesLevel === 'Senior'? 'checked' : 'unchecked'}
                                        onPress={() => chooseLevel('Senior')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseLevel('Middle')}>Senior</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }

    const chooseLevel = (param) => {
        setTextInputEmployeesLevel(param);
        setVisibleSelectLevel(false);
    }

    const selectEducation = () => {
        var modalBackgroundStyle = { backgroundColor: 'rgba(0, 0, 0, 0.5)' };
        var innerContainerTransparentStyle = { backgroundColor: '#fff', padding: 20, height: '50%', width: '80%', flexDirection: 'column', paddingBottom: '5%' };
        return (
            <Modal
                animationType='slide'
                transparent={true}
                visible={visibleSelectLastEducation}
                onRequestClose={false}
            >
                <View style={[styles.containerModal, modalBackgroundStyle]}>
                    <View style={innerContainerTransparentStyle}>
                        <Text style={{ textDecorationLine: 'underline' }} onPress={() => setVisibleSelectLastEducation(false)}>CLOSE</Text>
                        <View style={styles.list}>
                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='High School'
                                        status={textInputEmployeesLastEducation === 'High School'? 'checked' : 'unchecked'}
                                        onPress={() => chooseLastEducation('High School')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseLastEducation('High School')}>High School</Text>
                                </View>
                            </View>

                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Diploma'
                                        status={textInputEmployeesLastEducation === 'Diploma'? 'checked' : 'unchecked'}
                                        onPress={() => chooseLastEducation('Diploma')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseLastEducation('Diploma')}>Diploma</Text>
                                </View>
                            </View>

                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Bachelor'
                                        status={textInputEmployeesLastEducation === 'Bachelor'? 'checked' : 'unchecked'}
                                        onPress={() => chooseLastEducation('Bachelor')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseLastEducation('Bachelor')}>Bachelor</Text>
                                </View>
                            </View>

                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Magister'
                                        status={textInputEmployeesLastEducation === 'Magister'? 'checked' : 'unchecked'}
                                        onPress={() => chooseLastEducation('Magister')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseLastEducation('Magister')}>Magister</Text>
                                </View>
                            </View>

                            <View style={styles.listItem}>
                                <View style={styles.listLabel}>
                                    <RadioButton
                                        value='Doctor'
                                        status={textInputEmployeesLastEducation === 'Doctor'? 'checked' : 'unchecked'}
                                        onPress={() => chooseLastEducation('Doctor')}
                                    />
                                </View>
                                <View style={styles.listChoice}>
                                    <Text onPress={() => chooseLastEducation('Doctor')}>Doctor</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }

    const chooseLastEducation = (param) => {
        setTextInputEmployeesLastEducation(param);
        setVisibleSelectLastEducation(false);
    }


    const updateEmployees = async() => {
        try {
            const result = await axios.post('http://10.4.6.245/api/employees/'+textInputEmployeesId+'/update', {
                name: textInputEmployeesName,
                age: textInputEmployeesAge,
                role: textInputEmployeesRole,
                level: textInputEmployeesLevel,
                address: textInputEmployeesAddress,
                lastEducation: textInputEmployeesLastEducation,
                salary: textInputEmployeesSalary
            });


            if (result.data == 'Employees Update Successfuly') {
                alert(result.data);
                navigation.navigate('List');
            } else {
                alert(result.data);
            }
        } catch (err) {
            console.error(err);
        }

        // alert(textInputEmployeesId);

    }
    return (
        <SafeAreaView>
            <View style={styles.container}>
                <Text style={{ textDecorationLine: 'underline', fontWeight: 'bold' }}>EDIT EMPLOYEE</Text>
                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Name</Text>
                    </View>
                    <TextInput 
                    style={styles.textInput} 
                    value={textInputEmployeesName}
                    onChangeText={value => setTextInputEmployeesName(value)}
                    />
                </View>

                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Age</Text>
                    </View>
                    <TextInput 
                     style={styles.textInputNumber} 
                     keyboardType='number-pad' 
                     value={textInputEmployeesAge}
                     onChangeText={value => setTextInputEmployeesAge(value)}
                     />
                </View>

                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Role</Text>
                    </View>
                    <TouchableOpacity style={styles.inputPicker} onPress={() => setVisibleSelectRole(true)}>
                        <View style={styles.labelPicker}>
                            <Text>{textInputEmployeesRole}</Text>
                        </View>
                        <View style={styles.inputModal}>
                            <Icon name='chevron-down' size={hp(2.5)} />
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Level</Text>
                    </View>
                    <TouchableOpacity style={styles.inputPicker} onPress={() => setVisibleSelectLevel(true)}>
                        <View style={styles.labelPicker}>
                            <Text>{textInputEmployeesLevel}</Text>
                        </View>
                        <View style={styles.inputModal}>
                            <Icon name='chevron-down' size={hp(2.5)} />
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={styles.inputTextArea}>
                    <View style={styles.label}>
                        <Text>Address</Text>
                    </View>
                    <TextInput
                        style={styles.textArea}
                        multiline={true}
                        numberOfLines={4}
                        value={textInputEmployeesAddress}
                        onChangeText={value => setTextInputEmployeesAddress(value)}
                    />
                </View>

                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Last Education</Text>
                    </View>
                    <TouchableOpacity style={styles.inputPicker} onPress={() => setVisibleSelectLastEducation(true)}>
                        <View style={styles.labelPicker}>
                            <Text>{textInputEmployeesLastEducation}</Text>
                        </View>
                        <View style={styles.inputModal}>
                            <Icon name='chevron-down' size={hp(2.5)} />
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Salary</Text>
                    </View>
                    <TextInput 
                     style={styles.textInput}
                     value={textInputEmployeesSalary} 
                     onChangeText={value => setTextInputEmployeesSalary(value)}
                    />
                </View>

                <View style={styles.input}>
                    <View style={styles.label}>
                        <Text>Upload Photo</Text>
                    </View>
                    <Button style={styles.buttonUpload}>Select Photo</Button>
                </View>
                <View style={styles.inputButton}>
                    <Button 
                     style={styles.buttonUpdate} 
                     labelStyle={styles.labelButtonSave}
                     onPress={updateEmployees}
                     >
                         UPDATE
                    </Button>
                </View>

                {selectRole()}
                {selectLevel()}
                {selectEducation()}
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        width: wp(100),
        height: hp(100),
        backgroundColor: 'lightgrey',
        alignItems: 'center'
    },
    input: {
        width: wp(95),
        height: hp(10),
        marginTop: hp(0)
    },
    inputButton: {
        width: wp(95),
        height: hp(10),
        marginTop: hp(0),
        alignItems: 'flex-end'
    },
    inputPicker: {
        width: wp(95),
        height: hp(5),
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 1,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    inputTextArea: {
        width: wp(95),
        height: hp(11),
        marginTop: hp(2)
    },
    label: {
        width: wp(80),
        height: hp(3)
    },
    textInput: {
        width: wp(95),
        height: hp(5),
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 1,
        paddingLeft: wp(1)
    },
    textInputNumber: {
        width: wp(10),
        height: hp(5),
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 1
    },
    textArea: {
        width: wp(95),
        height: hp(8),
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: 1
    },
    labelPicker: {
        width: wp(80),
        height: hp(10),
        paddingLeft: wp(1),
    },
    inputModal: {
        width: wp(15),
        height: hp(10),
        alignItems: 'center'
    },
    containerModal: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        // paddingTop: Constants.statusBarHeight,
        backgroundColor: '#ecf0f1',
    },
    buttonUpload: {
        backgroundColor: 'violet'
    },
    buttonUpdate: {
        backgroundColor: 'cyan',
        width: wp(40)
    },
    labelButtonSave: {
        color: 'white'
    },
    list: {
        // backgroundColor:'red',
        height: hp(40)
    },
    listItem: {
        // backgroundColor:'green',
        height: hp(5),
        flexDirection: 'row'
    },
    listLabel: {
        // backgroundColor:'orange',
        width: '20%',
    },
    listChoice: {
        // backgroundColor:'salmon',
        width: '80%',
        paddingTop: '1.5%'
    }
});

export default EditEmployees;