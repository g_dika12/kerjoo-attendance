import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Modal,
  Platform,
  Alert,
  ActivityIndicator,
} from 'react-native';
import {TextInput, Button, RadioButton} from 'react-native-paper';
import {hp, wp} from './ResponsiveScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

function Login({navigation}) {
  const [textInputEmail, setTextInputEmail] = useState('');
  const [textInputPassword, setTextInputPassword] = useState('');
  const [accessToken, setAccessToken] = useState('');
  const [isLogin, setIsLogin] = useState(false);
  const [indicatorLoading, setIndicatorLoading] = useState(false);

  const loginUser = async () => {
    try {
      const result = await axios.post(
        'https://apitest.kerjoo.com/api/v1/auth',
        {
          email: textInputEmail,
          password: textInputPassword,
          // email: 'guitherez.git@gmail.com',
          // password: 't4u9dmyowW'
        },
      );

      // console.log(result.data);

      let token = result.data.access_token;

      setIndicatorLoading(true);

      if (token != null) {
        AsyncStorage.setItem('@access_token', token);
        if (!isLogin) {
          setIndicatorLoading(false);
          navigation.navigate('Attendance');
        }
      } else {
        alert('Login Gagal');
      }
    } catch (err) {
      console.error(err);
      alert(err);
    }
  };
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Text style={{textDecorationLine: 'underline', fontWeight: 'bold'}}>
          LOGIN
        </Text>
        <View style={styles.input}>
          <View style={styles.label}>
            <Text>Email</Text>
          </View>
          <TextInput
            style={styles.textInput}
            onChangeText={(value) => setTextInputEmail(value)}
          />
        </View>

        <View style={styles.input}>
          <View style={styles.label}>
            <Text>Password</Text>
          </View>
          <TextInput
            style={styles.textInput}
            onChangeText={(value) => setTextInputPassword(value)}
            secureTextEntry={true}
          />
        </View>

        <TouchableOpacity style={styles.inputButton} onPress={loginUser}>
          <Text style={styles.textLogin}>Login</Text>
        </TouchableOpacity>

        <ActivityIndicator
          size="large"
          color="#00ff00"
          animating={indicatorLoading}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp(100),
    height: hp(100),
    backgroundColor: 'lightgrey',
    alignItems: 'center',
  },
  input: {
    width: wp(95),
    height: hp(10),
    marginTop: hp(0),
  },
  inputButton: {
    width: wp(95),
    height: hp(5),
    marginTop: hp(0),
    alignItems: 'center',
    backgroundColor: 'red',
    justifyContent: 'center',
  },
  inputButtons: {
    width: wp(95),
    height: hp(5),
    marginTop: hp(0),
    alignItems: 'center',
    backgroundColor: 'green',
    justifyContent: 'center',
  },
  label: {
    width: wp(80),
    height: hp(3),
  },
  textInput: {
    width: wp(95),
    height: hp(5),
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
  },
  textInputNumber: {
    width: wp(10),
    height: hp(5),
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 1,
  },
  buttonLogin: {
    backgroundColor: 'green',
    width: wp(100),
  },
  textLogin: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: hp(2),
  },
});

export default Login;
