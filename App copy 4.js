import AsyncStorage from '@react-native-async-storage/async-storage'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import React, { Component } from 'react'
import { createContext } from 'react'
import { useMemo } from 'react'
import { useEffect } from 'react'
import { useReducer } from 'react'
import { Text, View } from 'react-native'
import HomeScreen1 from './src/basic auth/HomeScreen1'
import ProfileScreen1 from './src/basic auth/ProfileScreen1'
import ResetPassword1 from './src/basic auth/ResetPassword1'
import SettingScreen1 from './src/basic auth/SettingScreen1'
import SignInSceen1 from './src/basic auth/SignInSceen1'
import SignUpScreen1 from './src/basic auth/SignUpScreen1'

const AuthContext = createContext();
const Stack = createStackNavigator()

export default function App() {
  const [state, dispatch] = useReducer( // reducerSignin
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false
          }
        case 'SIGN_IN':
          return {
            ...prevState,
            userToken: action.token,
            isSignOut: false
          }
        case 'SIGN_OUT':
          return {
            ...prevState,
            userToken: null,
            isSignOut: true
          }
      }
    },
    {
      isLoading: true,
      isSignOut: false,
      userToken: null
    }
  )

  useEffect(() => {
    const bootDataAsync = async (/*dispatch */) => { // restoreTokenController
      let userToken;
      try {
        userToken = await AsyncStorage.getItem('userToken');
        console.log('--?> ', userToken)
        // dispatch({ type: 'RESTORE_TOKEN', token: userToken })
      } catch (error) {
        console.log(error)
      }
      dispatch({ type: 'RESTORE_TOKEN', token: userToken })
    }
    bootDataAsync()
  }, [])

  const authContext = useMemo(
    () => ({ // action controller
      signIn: async data => {
        dispatch({ type: 'SIGN_IN', token: "dummy-auth-token" })
      },
      signOut: () => dispatch({ type: 'SIGN_OUT' }),
      signUp: async data => {
        dispatch({ type: "SIGN_IN", token: "dummy-auth-token" })
      }
    }),
    []
  )
  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <Stack.Navigator>
          {
            state.userToken == null ?
              <>
                <Stack.Screen name="SignIn" component={SignInSceen1} />
                <Stack.Screen name="SignUp" component={SignUpScreen1} />
                <Stack.Screen name="ResetPassword" component={ResetPassword1} />
              </>
              :
              <>
                <Stack.Screen name="Home1" component={HomeScreen1} />
                <Stack.Screen name="Profile1" component={ProfileScreen1} />
                <Stack.Screen name="Settings1" component={SettingScreen1} />
              </>
          }
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
    // <View>
    //   <Text> textInComponent </Text>
    // </View>
  )
}